import sys

usage = 'USAGE: python cipher.py filename key method'
usage += '\n(example encrypt: python cipher.py test.txt 21 e)'
usage += '\n(example decrypt: python cipher.py test.txt 21 d)'

# make sure correct number of arguments are provided
if len(sys.argv) != 4:
	print usage
	sys.exit()

filename = sys.argv[1]
f = open(filename, 'r')

key = int(sys.argv[2])

# make sure method is correct
method = sys.argv[3]
if method == 'e':
	print 'encrypting'
elif method == 'd':
	print 'decrypting'
else:
	print 'unknown method, only encrypt (e) and decrypt (d) are valid methods'
	sys.exit()

content = f.read()


# get encryption/decryption
newcontent = ''
for c in content:
	code = ord(c)
	if method == 'e':
		code += key
	else:
		code -= key
	newc = chr(code)
	newcontent += newc

# write to the file
f.close()
f = open(filename, 'w')
f.write(newcontent)
