USAGE: python cipher.py filename key method
example encrypt: python cipher.py test.txt 21 e
example decrypt: python cipher.py test.txt 21 d

copyright Jameson Saunders
